from flask import render_template, request
from app import app

@app.route('/')
def index():
    return render_template("login.html")
@app.route('/home')
def home():
  pageData = {
      "breadcrumb" : "Dashboard",
      "pageHeader" : "Dashboard",
      "pages" : "dashboard.html"
  }
  return render_template("index.html", pageData=pageData)